# First Pass Analysis
To run "first pass" analysis using the 2015 pilot data:

1. Get data. I have not determined a good way to download this data on the command line, so I have resorted to downloading the following files, then transfering them to the VM
      - [dryrun_combined_I1.fastq.gz](https://duke.box.com/s/ha7hqlv433qg4mwv1bonfaollyedsess)
      - [dryrun_combined_R1.fastq.gz](https://duke.box.com/s/1w7r4pljuu0fxx1yvwszacc76aem2g8w)
      - [dryrun_combined_R2.fastq.gz](https://duke.box.com/s/0uzhixwn6v4qzk2ppqxdzv0exbytcfjc)
2. Put raw data in a directory on the VM that will be mounted in the docker container. For example I have put the raw data in `/home/bitnami/scratch/ngs2016_test/raw_data/dryrun_data`.  Since I will be mounting `/home/bitnami/scratch` to `/home/jovyan/work/scratch` in the docker container, this subdirectory is included.
3. Lauch the docker image, setting up environmental variables and mounting appropriate host directories:
~~~
docker run -d -p 8889:8888 -e PASSWORD="Choose_a_password_and_put_it_here" -e USE_HTTPS=yes  \
       -v /home/bitnami/bioinf_nb_ngscourse2015:/home/jovyan/work \
       -v /home/bitnami/scratch:/home/jovyan/work/scratch  \
       --env BASE=/home/jovyan/work \
       --env OUTPUT=/home/jovyan/work/scratch/ngs2016_test/output/one_sample \
       --env DATA=/home/jovyan/work/scratch/ngs2016_test/raw_data/dryrun_data \
       --name ngs1 \
       janicemccarthy/dukehtscourse start-notebook.sh
~~~
  - BASE : The top directory of the course repo
  - OUTPUT : Path for pipeline output
  - DATA : Path to the data where pilot data was downloaded
4. Open https://YOUR_VM_URL:8889/ in a webbrowser (preferably Chrome), replacing *YOUR_VM_URL* with the URL for your virtual machine.
5. In Jupyter, open the notebook *bioinformatics_toc.ipynb*.
6. Follow the steps in the section *Introduction to Bioinformatics Pipeline*

**Note:** In order to shutdown and restart the Jupyter server, do `docker stop ngs1; docker rm ngs1` or `docker rm --fore ngs1` before relaunching with the above `docker run ...` command.
